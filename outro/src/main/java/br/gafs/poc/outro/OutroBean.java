package br.gafs.poc.outro;

import javax.inject.Named;

@Named
public class OutroBean {

    private String valor = "Outro MBean " + System.currentTimeMillis();

    public String getValor() {
        return valor;
    }
}
