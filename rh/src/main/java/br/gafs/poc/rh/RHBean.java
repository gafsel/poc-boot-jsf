package br.gafs.poc.rh;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named("rhBean")
public class RHBean {

    private String valor = "RH " + System.currentTimeMillis();

    public String getValor() {
        return valor;
    }
}
