package br.gafs.poc.app;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "br.gafs.poc")
public class AppConfiguration {

}
